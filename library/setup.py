from library.dispersionplots import *

from dash import dcc
import dash_bootstrap_components as dbc
from dash import html

from dash.dependencies import Input, Output

def layout(debug=False):

    # see https://dash-bootstrap-components.opensource.faculty.ai/docs/components/form/ for general information and https://getbootstrap.com/docs/4.3/utilities/spacing/ for className choices
    return dbc.Container(
        [
            dbc.Col(
                [
                    dbc.Card(
                        dbc.CardBody(
                            [
                                html.H1("Phasen- & Gruppengeschwindigkeit"), # add soft hyphens via \xad
                                html.Hr(),
                                html.P("In dieser Simulation wird die Ausbreitung mehrer überlagerter sinusförmiger Wellen animiert, um die Unterschiede zwischen Phasen- und Gruppengeschwindigkeit darzustellen. Hierzu können zwischen 2 und 10 Schwingungen überlagert werden, die eine normierte Frequenz zwischen 1 und 50, sowie eine relative Phasenverschiebung zwischen 0 und 360 Grad aufweisen können. Die Phasen- und Gruppengeschwindigkeit können im Bereich von 0.5 und 2 variiert werden und sind ebenfalls normiert."),
                                html.P(r"Die einzelnen Schwingungen \(E_i\) sind dabei definiert als \(E_i=\cos(\omega_i t - \beta_i z)\), woraus sich ein Gesamtpuls \(E_\text{res}=\sum_i E_i = E_\text{E} E_\text{T}\) ergibt."),
                                html.P(r"Die Trägerschwingung berechnet sich zu \(E_\text{T} = \cos(\omega_\text{T}(t-\frac{z}{v_\text{ph}}))\), wobei \(\omega_\text{T}\) die mittlere Frequenz und \(v_\text{ph}=\frac{\omega_\text{T}}{\beta_\text{T}}\) die Phasengeschwindigkeit bezeichnet."),
                                html.P(r"Die Einhüllende berechnet sich für den Fall von zwei Schwingungen aus \(E_\text{E}=\cos(\Delta \omega (t - \frac{z}{v_\text{gr}}))\), wobei \(\Delta \omega = \omega_{w_{i+1}}-\omega_{w_i}\) und \(v_\text{gr} = \frac{\Delta \omega}{\Delta k} \) ist. Diese Näherung gilt jedoch nur für \(\Delta \omega \ll \omega_\text{T}\)."), 
                                html.P("Zur Berechnung der Einhüllenden im Falle von mehr als zwei Schwingungen wird die Hilbert-Transformation verwendet."),
                                html.Hr(),
                            ],
                        ),
                        style={'border': 'none'},
                    )
                ],
            ),

            dbc.Row(
                [
                    dbc.Col(
                        [              
                                dcc.Graph(id='dispersion-graph', config=dict(showLink=debug, plotlyServerURL="https://chart-studio.plotly.com", toImageButtonOptions = dict(format='svg'))),
                                dcc.Interval(id='interval-component',disabled=True,interval=100, # in milliseconds
                                                n_intervals=0)                   
                        ]
                        , width=6
                    ),
                    dbc.Col(
                        [
                                dcc.Graph(id='components-graph', config=dict(showLink=debug, plotlyServerURL="https://chart-studio.plotly.com", toImageButtonOptions = dict(format='svg'))),                
                        ]
                        , width=6
                    ),
                ]
            ),
            #--------------------------------------------------------------------------
            # ANIMATION SETTINGS
            #--------------------------------------------------------------------------
            html.Hr(),
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.Div( 
                                dbc.Button(
                                    id='play',
                                    children='Play', 
                                    color='primary', 
                                    className='me-1',
                                    size='lg',
                                    style={'width': '100px'}
                                )
                            )
                        ], width=1
                    ),
                    dbc.Col(
                        [
                            html.Div( 
                                dbc.Button(
                                    id='reset',
                                    children='Reset', 
                                    color='secondary', 
                                    className='me-1',
                                    size='lg',
                                    style={'width': '100px'}
                                )
                            )
                        ], width=1
                    ),
                    dbc.Col(
                        [
                            html.Div(
                                [   
                                    dbc.Label("Animationsgeschwindigkeit", html_for="slider"),
                                    dcc.Slider(
                                        id="speed",
                                        min=0.1,
                                        max=10,
                                        step=0.1,
                                        value=1,
                                        marks={
                                            0.1: {'label': '0.1'},
                                            1: {'label': '1'},
                                            5: {'label': '5'},
                                            10: {'label': '10'},
                                        },
                                        included=False
                                    ),
                                ],
                            )
                        ], width=4
                    ),
                    html.Br(),
                ]
            ),
            html.Hr(),
            #--------------------------------------------------------------------------
            # PHASE/GROUP VELOCITY AND FREQUENCIES
            #--------------------------------------------------------------------------
            dbc.Row(
                [    
                    dbc.Col(
                        [
                            dbc.Label("Anzahl Schwingungen"),
                            dbc.InputGroup(
                                [
                                    dbc.Input(id='n_f-sel', type='number', value=2, min=2, max=10, step=1, style={'maxWidth': '6em'}),
                                ],
                            ),
                        ],
                        width="2",
                        className="mb-3",
                    ),
                     dbc.Col(
                        [
                            html.Div(
                                [   
                                    dbc.Label("Phasengeschwindigkeit", html_for="slider"),
                                    dcc.Slider(
                                        id="f_ph-sel",
                                        min=0.5,
                                        max=2,
                                        step=0.1,
                                        value=1,
                                        marks={
                                            0.5: {'label': '0.5'},
                                            0.75: {'label': '0.75'},
                                            1: {'label': '1'},
                                            1.5: {'label': '1.5'},
                                            2: {'label': '2'},
                                        },
                                        included=False
                                    ),
                                ],
                            )
                        ],
                        width=4
                    ),
                    dbc.Col(
                        [
                            html.Div(
                                [   
                                    dbc.Label("Gruppengeschwindigkeit", html_for="slider"),
                                    dcc.Slider(
                                        id="f_gr-sel",
                                        min=0.5,
                                        max=2,
                                        step=0.1,
                                        value=1,
                                        marks={
                                            0.5: {'label': '0.5'},
                                            0.75: {'label': '0.75'},
                                            1: {'label': '1'},
                                            1.5: {'label': '1.5'},
                                            2: {'label': '2'},
                                        },
                                        included=False
                                    )
                                ],
                            )
                        ],
                        width=4
                    ),
                    #--------------------------------------------------------------------------
                    # FREQUENCIES
                    #--------------------------------------------------------------------------
                    dbc.Col(
                        [
                            dbc.InputGroup(
                                id='frequencies', children=[
                                dbc.Col([
                                    dbc.Label("Frequenz " + str(i+1)),
                                    dbc.InputGroup(
                                        [
                                            dbc.InputGroupText("f"),
                                            dbc.Input(id='f'+str(i+1), type='number', value=15+i, min=1, max=50, step=0.1, style={'maxWidth': '5em'}),
                                            dbc.InputGroupText(r"\(\varphi\)"),
                                            dbc.Input(id='phase'+str(i+1), type='number', value=0, min=0, max=360, step=1, style={'maxWidth': '5em'}),
                                        ],
                                    ),
                                        ]
                                ) for i in range(10)]
                            ),
                        ],
                        width="auto",
                        className="mb-3",
                    ),
                    
                ]
            ),

        ],
        fluid=True
    )


def callbacks(app):

    # animation of play button
    @app.callback([Output('play', 'color'),Output('play','active'),Output('interval-component','disabled'),Output('play','children')], [Input('play', 'n_clicks'),Input('play','active')])
    def change_button_style(n_clicks,active):
        if n_clicks:
            active = not active
        if active:
            return 'primary', active, not active, 'Pause'
        else:
            return 'secondary', active, not active, 'Play'

    # dispersion plot
    @app.callback(
        [Output('dispersion-graph', 'figure'),
        Output('components-graph', 'figure')],
        [Input('f_ph-sel','value'),
        Input('f_gr-sel','value'),
        Input('interval-component','n_intervals'),
        [[Input('f'+str(i+1),'value') for i in range(10)],[Input('phase'+str(i+1),'value') for i in range(10)]],
        Input('n_f-sel','value'),
        Input('speed', 'value'),
        ],
    )
    def update_dispersion_plot(F_ph,F_gr,n_intervals,frequencies,n_f,speed):
        '''
            Function to update the dispersion plot and the component plot.

            Args:
                F_ph (int): factor of phase velocity
                F_gr (int): factor of group velocity
                n_intervals (int): frame number (generated by dcc.interval function in applayout.py)
                freqeuencies (list): 10x2 list with all frequencies and their respective phase offset
                n_f (int): number of currently used basic frequencies
                speed (int): animation speed

            Returns:
                fig1 (object): go.Figure object with a plot of impulse, envelope, carrier frequency, phase velocity pointer and group velocity pointer
                fig2 (object): go.Figure object with a plot of basic frequencies
        '''
        fig1,fig2 = generate_figure(F_ph,F_gr,n_intervals,frequencies,n_f,speed)
        return fig1,fig2

    # frequencies selection
    @app.callback(
        [Output('frequencies', 'children'),
        Output('interval-component', 'n_intervals'),],
        [Input('n_f-sel','value'),
        [Input('f'+str(i+1),'value') for i in range(10)],
        [Input('phase'+str(i+1),'value') for i in range(10)],
        Input('f_ph-sel','value'),
        Input('f_gr-sel','value'),
        Input('reset', 'n_clicks')
        ],
    )
    def frequenciesSel(n_f,frequency,phase,F_ph,F_gr,rst):
        '''
        Gives back a list with all possible frequency input objects and resets n_intervals.

        Args:
            n_f (int): number of visible frequencies 
            frequency (list): currently choosen frequencies
            phase (list): currently choosen phases
            F_ph, F_gr, rst (int): only used to trigger callback to reset n_intervals

        Returns:
            input (list): updated frequency list
            (int): return 0 to reset n_intervals
        '''
        input = []
        for i in range(10):
            if i < n_f:
                input.append(
                    dbc.Col([
                        dbc.Label("Frequenz " + str(i+1)),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("f"),
                                dbc.Input(id='f'+str(i+1), type='number', value=frequency[i], min=1, max=50, step=0.1, style={'maxWidth': '5em'}),
                                dbc.InputGroupText(r"\(\varphi\)"),
                                dbc.Input(id='phase'+str(i+1), type='number', value=phase[i], min=0, max=360, step=1, style={'maxWidth': '5em'}),
                            ],
                        ),
                    ],
                    width="auto",
                    className="mb-3",
                    )
                )
            else:
                input.append(
                    dbc.Col([
                        dbc.Label("Frequenz " + str(i+1)),
                        dbc.InputGroup(
                            [
                                dbc.InputGroupText("f"),
                                dbc.Input(id='f'+str(i+1), type='number', value=15+i, min=1, max=50, step=0.1, style={'maxWidth': '5em'}),
                                dbc.InputGroupText(r"\(\varphi\)"),
                                dbc.Input(id='phase'+str(i+1), type='number', value=0, min=0, max=360, step=1, style={'maxWidth': '5em'}),
                            ],
                        ),
                    ],
                    width="auto",
                    className="invisible",
                    )
                )
        return input, 0

    return app